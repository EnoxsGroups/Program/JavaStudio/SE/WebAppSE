package enoxs.com.data.gen.dao;

import enoxs.com.data.gen.model.AppInfo;
import enoxs.com.util.MyBatisUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class AppInfoMapperTest {

    AppInfoMapper appInfoMapper = null;

    @Before
    public void setUp() throws Exception {
        MyBatisUtil.start("mybatis-config.xml");
        MyBatisUtil.sqlSession.getMapper(AppInfoMapper.class);
        appInfoMapper = MyBatisUtil.sqlSession.getMapper(AppInfoMapper.class);
    }

    @After
    public void tearDown() throws Exception {
        MyBatisUtil.done();
    }


    @Test
    public void deleteByPrimaryKey() {
    }

    @Test
    public void insert() {
    }

    @Test
    public void selectByPrimaryKey() {
        Integer id = 1;
        AppInfo appInfo = appInfoMapper.selectByPrimaryKey(id);
    }

    @Test
    public void selectAll() {
        List<AppInfo> lstAppInfo = appInfoMapper.selectAll();
        assertTrue(lstAppInfo.size() > 0);
    }

    @Test
    public void updateByPrimaryKey() {
    }

}