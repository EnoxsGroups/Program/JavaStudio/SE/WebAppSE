package enoxs.com.data.dao;

import enoxs.com.util.MyBatisUtil;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class InitSQLiteDbMapperTest {
    protected static InitSQLiteDbMapper initSQLiteDbMapper = null;

    @BeforeClass
    public static void beforeClass(){
        MyBatisUtil.start("mybatis/mybatis-config.xml");
        MyBatisUtil.sqlSession.getMapper(InitSQLiteDbMapper.class);
        initSQLiteDbMapper = MyBatisUtil.sqlSession.getMapper(InitSQLiteDbMapper.class);
    }

    @AfterClass
    public static void afterClass(){
        MyBatisUtil.done();
    }

    @Test
    public void dropAllTable(){
        dropAppInfoTable();
        dropUserInfoTable();
        dropAppGroupTable();
        dropAppSubGroupTable();
        dropAppGroupProjectTable();
    }

    @Test
    public void createAllTable() {
        createAppInfoTable();
        createUserInfoTable();
        createAppGroupTable();
        createAppSubGroupTable();
        createAppGroupProjectTable();
    }

    @Test
    public void insertAllData(){
        insertAppInfoData();
        insertUserInfoData();
        insertAppGroupData();
        insertAppSubGroupData();
        insertAppGroupProjectData();
    }

    @Test
    public void dropAppInfoTable(){
        initSQLiteDbMapper.dropAppInfoTable();
    }

    @Test
    public void dropUserInfoTable(){
        initSQLiteDbMapper.dropUserInfoTable();
    }

    @Test
    public void dropAppGroupTable(){
        initSQLiteDbMapper.dropAppGroupTable();
    }

    @Test
    public void dropAppSubGroupTable(){
        initSQLiteDbMapper.dropAppSubGroupTable();
    }

    @Test
    public void dropAppGroupProjectTable(){
        initSQLiteDbMapper.dropAppGroupProjectTable();
    }

    @Test
    public void createAppInfoTable(){
        initSQLiteDbMapper.createAppInfoTable();
    }

    @Test
    public void createUserInfoTable(){
        initSQLiteDbMapper.createUserInfoTable();
    }

    @Test
    public void createAppGroupTable(){
        initSQLiteDbMapper.createAppGroupTable();
    }

    @Test
    public void createAppSubGroupTable(){
        initSQLiteDbMapper.createAppSubGroupTable();
    }

    @Test
    public void createAppGroupProjectTable(){
        initSQLiteDbMapper.createAppGroupProjectTable();
    }

    @Test
    public void insertAppInfoData(){
        initSQLiteDbMapper.insertAppInfoData();

        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_info");
        }};
        Integer count = initSQLiteDbMapper.selectCount(map);
        assertTrue(count > 0);
    }

    @Test
    public void insertUserInfoData(){
        initSQLiteDbMapper.insertUserInfoData();

        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "user_info");
        }};
        Integer count = initSQLiteDbMapper.selectCount(map);
        assertTrue(count > 0);
    }

    @Test
    public void insertAppGroupData(){
        initSQLiteDbMapper.insertAppGroupData();

        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_group");
        }};
        Integer count = initSQLiteDbMapper.selectCount(map);
        assertTrue(count > 0);
    }

    @Test
    public void insertAppSubGroupData(){
        initSQLiteDbMapper.insertAppSubGroupData();

        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_sub_group");
        }};
        Integer count = initSQLiteDbMapper.selectCount(map);
        assertTrue(count > 0);
    }

    @Test
    public void insertAppGroupProjectData(){
        initSQLiteDbMapper.insertAppGroupProjectData();

        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_group_project");
        }};
        Integer count = initSQLiteDbMapper.selectCount(map);
        assertTrue(count > 0);
    }

    @Test
    public void selectOneMapResult(){
        Long id = Long.valueOf(1);
        initSQLiteDbMapper.selectOneMapResult(id);
    }

    @Test
    public void selectMapResult() {
        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_info");
            put("id", "1");
        }};
        initSQLiteDbMapper.selectMapResult(map);
    }

    @Test
    public void selectCount(){
        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_info");
        }};

        Integer count = initSQLiteDbMapper.selectCount(map);
        System.out.println("count : " + count);
    }
}