package enoxs.com.data.dao;

import enoxs.com.data.gen.dao.AppInfoMapper;
import enoxs.com.data.gen.model.AppInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InitSQLiteDbMapper extends AppInfoMapper {
    void dropAppInfoTable();
    void dropUserInfoTable();
    void dropAppGroupTable();
    void dropAppSubGroupTable();
    void dropAppGroupProjectTable();

    void createAppInfoTable();
    void createUserInfoTable();
    void createAppGroupTable();
    void createAppSubGroupTable();
    void createAppGroupProjectTable();

    void insertAppInfoData();
    void insertUserInfoData();
    void insertAppGroupData();
    void insertAppSubGroupData();
    void insertAppGroupProjectData();

    /**
     * annotation parameter
     */
    List<Map<String,Object>> selectOneMapResult(@Param(value="id") Long id);

    /**
     * dynamic query table and parameter
     */
    List<Map<String,Object>> selectMapResult(Map<String, String> condition);
    Integer selectCount(Map<String, String> condition);
}
