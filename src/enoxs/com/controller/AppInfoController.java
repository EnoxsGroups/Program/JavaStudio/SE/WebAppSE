package enoxs.com.controller;

import enoxs.com.app.info.AppInfo;
import enoxs.com.app.info.AppInfoService;
import enoxs.com.util.JSONUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Controller
@RequestMapping({"/appInfo"})
public class AppInfoController {
    // Enoxs To-Do : Post Long : Query id
    // Enoxs To-Do : Post String : Query Name
    // Enoxs To-Do : Post More Parameter : Query id and Name
    // Enoxs To-Do : Post Integer Array : Query Muilt Position
    // Enoxs To-Do : Post String Array : Query Muily Name
    // Enoxs To-Do : Post Long List : Query Muilt Id
    // Enoxs To-Do : Post Object : Query Object
    // Enoxs To-Do : Test Page : Auto Test
    // Enoxs To-Do : JSONUtil 單元測試

    //Enoxs TO-DO : POST List<Object>
    //Enoxs TO-DO : POST Object include List<>

    @Autowired
    AppInfoService appInfoService;

    @RequestMapping(value = {""}, method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView view = new ModelAndView("app/appInfo");
        return view;
    }

/*
    @RequestMapping(value = {"qunitSE"}, method = RequestMethod.GET)
    public ModelAndView qunitSE() {
        ModelAndView view = new ModelAndView("qunit/qunitSE");
        return view;
    }
*/

    /**
     * Demo:
     * Transfer 1 parameter (Integer)
     */
    @ResponseBody
    @RequestMapping(value = {"/queryAppInfoByPosition"}, method = RequestMethod.POST)
    public String queryAppInfoByPosition(Integer position) {

        System.out.println("queryAppInfoByPosition :" + position);

        JSONObject result = JSONUtil.generateAjaxResult("success", null);
        try {
            AppInfo appInfo = appInfoService.queryAppInfoByPosition(position);
            result.put("list", new JSONObject(appInfo));

        } catch (Exception e) {
            e.printStackTrace();
            result = JSONUtil.generateAjaxResult(null, "fail");
        }
        return JSONUtil.encodeString(result.toString());
    }

    /**
     * Demo:
     * Transfer 1 parameter (Long)
     */
    @ResponseBody
    @RequestMapping(value = {"/queryAppInfoById"}, method = RequestMethod.POST)
    public String queryAppInfoById(Long id) {

        System.out.println("queryAppInfoById :" + id);

        JSONObject result = JSONUtil.generateAjaxResult("success", null);
        try {
            AppInfo appInfo = appInfoService.queryAppInfoById(id);
            result.put("list", new JSONObject(appInfo));
        } catch (Exception e) {
            e.printStackTrace();
            result = JSONUtil.generateAjaxResult(null, "fail");
        }
        return JSONUtil.encodeString(result.toString());
    }

    /**
     * Demo:
     * Transfer 1 parameter (String)
     */
    @ResponseBody
    @RequestMapping(value = {"/queryAppInfoByName"}, method = RequestMethod.POST)
    public String queryAppInfoByName(String name) {

        System.out.println("queryAppInfoByName :" + name);

        JSONObject result = JSONUtil.generateAjaxResult("success", null);
        try {
            AppInfo appInfo = appInfoService.queryAppInfoByName(name);
            result.put("list", new JSONObject(appInfo));
        } catch (Exception e) {
            e.printStackTrace();
            result = JSONUtil.generateAjaxResult(null, "fail");
        }
        return JSONUtil.encodeString(result.toString());
    }

    /**
     * Demo:
     * Transfer 2 parameter (Long , String)
     */
    @ResponseBody
    @RequestMapping(value = {"/queryAppInfoByIdAndName"}, method = RequestMethod.POST)
    public String queryAppInfoByIdAndName(Long id, String name) {

        System.out.println("queryAppInfoByIdAndName :" + name);
        System.out.println("Id : " + id);
        System.out.println("Name : " + name);

        JSONObject result = JSONUtil.generateAjaxResult("success", null);
        try {
            AppInfo appInfo = appInfoService.queryAppInfoByIdAndName(id, name);
            result.put("list", new JSONObject(appInfo));
        } catch (Exception e) {
            e.printStackTrace();
            result = JSONUtil.generateAjaxResult(null, "fail");
        }
        return JSONUtil.encodeString(result.toString());
    }

    /**
     * Demo:
     * Transfer Multi - parameter (Integer [] )
     */
    @ResponseBody
    @RequestMapping(value = {"/queryAppInfoByMultiPostion"}, method = RequestMethod.POST)
    public String queryAppInfoByMultiPostion(@RequestParam(name = "positions[]", required = false) Integer[] positions) {

        System.out.println("queryAppInfoByMultiPostion :" + Arrays.toString(positions));

        JSONObject result = JSONUtil.generateAjaxResult("success", null);
        try {
            List<AppInfo> lstAppInfo = appInfoService.queryAppInfoByMultiPostion(positions);
            result.put("list", new JSONArray(lstAppInfo));
        } catch (Exception e) {
            e.printStackTrace();
            result = JSONUtil.generateAjaxResult(null, "fail");
        }
        return JSONUtil.encodeString(result.toString());
    }

    /**
     * Demo:
     * Transfer Multi - parameter (String [] )
     */
    @ResponseBody
    @RequestMapping(value = {"/queryAppInfoByMultiName"}, method = RequestMethod.POST)
    public String queryAppInfoByMultiName(@RequestParam(name = "names[]", required = false) String[] names) {

        System.out.println("queryAppInfoByMultiName :" + Arrays.toString(names));

        JSONObject result = JSONUtil.generateAjaxResult("success", null);
        try {
            List<AppInfo> lstAppInfo = appInfoService.queryAppInfoByMultiName(names);
            result.put("list", new JSONArray(lstAppInfo));
        } catch (Exception e) {
            e.printStackTrace();
            result = JSONUtil.generateAjaxResult(null, "fail");
        }
        return JSONUtil.encodeString(result.toString());
    }

    /**
     * Demo:
     * Transfer Multi - parameter (List<Integer>)
     * Reply -> Map<String,Object>
     */
    @ResponseBody
    @RequestMapping(value = {"/queryAppInfoByMultiId"}, method = RequestMethod.POST)
    public Map<Long, Object> queryAppInfoByMultiId(@RequestBody List<Long> lstId) {
        Map<Long, Object> resultMap = new LinkedHashMap<>();
        try {
            List<AppInfo> lstAppInfo = appInfoService.queryAppInfoByMultiId(lstId);
            for (AppInfo appInfo : lstAppInfo) {
                resultMap.put(appInfo.getAppId(), appInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
            resultMap.put(Long.valueOf(500), e.getMessage());
        }
        return resultMap;
    }

    /**
     * Demo:
     * Transfer Object - parameter (AppInfo)
     * Reply -> Object (AppInfo)
     */
    @ResponseBody
    @RequestMapping(value = {"/queryAppInfoByAppInfoObject"}, method = RequestMethod.POST)
    public AppInfo queryAppInfoByAppInfoObject(@RequestBody AppInfo appInfo) {
        AppInfo mAppInfo = null;
        try {
            mAppInfo = appInfoService.queryAppInfoByAppInfoObject(appInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mAppInfo;
    }
}
