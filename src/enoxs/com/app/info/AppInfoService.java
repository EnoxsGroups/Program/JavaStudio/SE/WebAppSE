package enoxs.com.app.info;

import org.springframework.stereotype.Service;

import java.util.List;

public interface AppInfoService {
    AppInfo queryAppInfoByPosition(Integer position);

    AppInfo queryAppInfoById(Long id);

    AppInfo queryAppInfoByName(String name);

    AppInfo queryAppInfoByIdAndName(Long id , String name);

    List<AppInfo> queryAppInfoByMultiPostion(Integer [] positions);

    List<AppInfo> queryAppInfoByMultiName(String [] names);

    List<AppInfo> queryAppInfoByMultiId(List lstId);

    AppInfo queryAppInfoByAppInfoObject(AppInfo appInfo);
}
